{{/*The name of the chart that will be used as a prefix object names*/}}
{{- define "sagbot.apigw.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end -}}

{{/* The name + version of the helm chart */}}
{{- define "sagbot.apigw.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "sagbot.apigw.containerPort" -}}
{{- default .Values.containerPort 80 }}
{{- end }}

{{- define "sagbot.apigw.servicePort" -}}
{{- default .Values.servicePort 80 }}
{{- end }}

{{- define "sagbot.apigw.imageRepository" -}}
{{- default .Values.imageRepository "registry.gitlab.com/sagbot/auth-demo/apigw" }}
{{- end }}

{{- define "sagbot.apigw.imageTag" -}}
{{- default .Values.imageTag "master" }}
{{- end }}

{{- define "sagbot.apigw.imagePullPolicy" -}}
{{- default .Values.imagePullPolicy "IfNotPresent" }}
{{- end }}





{{/*Global annotations for every object*/}}
{{- define "sagbot.apigw.globalAnnotations" -}}
sagbot/author: "Sagiv Oulu"
sagbot/chart: api-gateway
{{- end -}}

{{/*Global labels for every object*/}}
{{- define "sagbot.apigw.globalLabels" -}}
sagbot/chart: api-gateway
{{- end -}}

{{- define "sagbot.apigw.pullUsingCredentials" -}}
{{- if .Values.image.credentials -}}
true
{{- else -}}
false
{{- end -}}
{{- end -}}