# APIGW

```APIGW``` is an application rest api gateway, besed on nginx with middleware capabilities.

## TL;DR;

```console
$ helm install apigw sagbot/apigw
```

## Introduction

This chart deploys an nginx deployment, with nginx common configurations supported from the ```values.yaml``` file. This nginx comes with lua support for adding request middleware (for example: authentication, authorization, custom request logic etc...)

## Installing the Chart

To install the chart with the release name `apigw`:

```console
$ helm install apigw sagbot/apigw
```

## Uninstalling the Chart

To uninstall the `apigw` deployment:

```console
$ helm uninstall apigw
```

## Configuration

The following table lists the configurable parameters of the apigw chart and their default values.

| Parameter                     | Description                                                               | Default                                       |
|-------------------------------|---------------------------------------------------------------------------|-----------------------------------------------|
| `nameOverride`                | Optionally override the name                                              | `""`                                          |
| `replicas`                    | The number of api gateway replicas to create                              | `1`                                           |
| `image.repository`            | The api gateway image repository                                          | `registry.gitlab.com/sagbot/auth-demo/apigw`  |
| `image.tag`                   | Overrides the api gateway image tag whose default is the chart version    | `"master"`                                    |
| `image.pullPolicy`            | The apigw image pull policy                                            | `IfNotPresent`                                |
| `imageCredentials.registry`   | The image registry to authenticate against                                | `"https://registry.gitlab.com"`               |
| `imageCredentials.username`   | The username to use when pulling the image                                | `""`                                          |
| `imageCredentials.password`   | The password to use when pulling the image                                | `""`                                          |
| `imageCredentials.email`      | The email to use when pulling the image (can leave as default)            | `""`                                          |
| `servicePort`                 | The port the service will listen on                                       | `80`                                          |
| `containerPort`               | The port that the container listens on                                    | `80`                                          |
| `ingress.enabled`             | If `true`, an Ingress is created                                          | `false`                                       |
| `ingress.hostname`            | The hostname of the ingress                                               | `""`                                          |
| `ingress.tls.enabled`         | If `true`, the Ingress will serve using a tls certificate (https)         | `true`                                        |
| `ingress.tls.secretName`      | If specified, this secret will be used as a tls certificate               | `""`                                          |
| `ingress.tls.crt`             | The content of the certificate public key to use                          | `""`                                          |
| `ingress.tls.key`             | The content of the certificate private key to use                         | `""`                                          |
| `routes`                      | A list of api gateway routes                                              | `[]`                                          |
| `routes[].uri`                | the uri of the request the will match this route                          | `""`                                          |
| `routes[].response`           | Static response.  the text to return                                      | `""`                                          |
| `routes[].responseCode`       | Static response.  the response code to return                             | `200`                                         |
| `routes[].upstreamUrl`        | Proxy.            the upstream url to proxy to                            | `""`                                          |
| `routes[].upstreamUri`        | Proxy.            the upstream uri to proxy to                            | `/`                                           |
| `routes[].scriptName`         | If specified, this script will run before every request                   | `""`                                          |
| `scripts`                     | A list of scripts to add to the api gateway service                       | `[]`                                          |
| `scripts[].name`              | The name of the script                                                    | `""`                                          |
| `scripts[].script`            | The lua code of the script                                                | `""`                                          |
| `podAnnotations`              | Annotations to give all created pods                                      | `{}`                                          |
| `podLabels`                   | Labels to give all created pods                                           | `{}`                                          |
| `deployentAnnotations`        | Annotations to give created deployent                                     | `{}`                                          |
| `deployentLabels`             | Labels to give created deployent                                          | `{}`                                          |
| `serviceAnnotations`          | Annotations to give created service                                       | `{}`                                          |
| `serviceLabels`               | Labels to give created service                                            | `{}`                                          |
| `ingressAnnotations`          | Annotations to give created ingress                                       | `{}`                                          |
| `ingressLabels`               | Labels to give created ingress                                            | `{}`                                          |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`. For example:

```console
$ helm install apigw sagbot/apigw --set replicas=1
```

Alternatively, a YAML file that specifies the values for the parameters can be provided while
installing the chart. For example:

```console
$ helm install apigw sagbot/apigw --values values.yaml
```

In order to get started, you can use the example [```values.yaml``` file](https://gitlab.com/sagbot/helm-repo/-/raw/master/charts/apigw/example-values.yaml)
