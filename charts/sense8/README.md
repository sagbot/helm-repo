# Sense8

```Sense8``` is a simple blackbox monitoring tool. It samples your system using generic bash scripts that run on intervals, & sends their output to elasticsearch.

## TL;DR;

```console
$ helm install sense8 sagbot/sense8
```

## Introduction

This chard creates a fluentd deployment that is pre baked with the needed configuration for running you scripts on intervals, parsing there output & sending it to elasticsearch.

## Installing the Chart

To install the chart with the release name `sense8`:

```console
$ helm install sense8 sagbot/sense8
```

## Uninstalling the Chart

To uninstall the `sense8` deployment:

```console
$ helm uninstall sense8
```

## Configuration

The following table lists the configurable parameters of the sense8 chart and their default values.

| Parameter                                   | Description                                                               | Default                                       |
|---------------------------------------------|---------------------------------------------------------------------------|-----------------------------------------------|
| `nameOverride`                              | Optionally override the name                                              | `""`                                          |
| `image.repository`                          | The fluentd image repository                                              | `"registry.gitlab.com/sagbot/blackbox-docker"`|
| `image.tag`                                 | Overrides the fluentd image tag whose default is the chart version        | `"v1.12.4-debian-debian-1.0"`                 |
| `image.pullPolicy`                          | The Fluentd image pull policy                                             | `"IfNotPresent"`                              |
| `imageCredentials.registry`                 | The image registry to authenticate against                                | `"https://registry.gitlab.com"`               |
| `imageCredentials.username`                 | The username to use when pulling the image                                | `""`                                          |
| `imageCredentials.password`                 | The password to use when pulling the image                                | `""`                                          |
| `imageCredentials.email`                    | The email to use when pulling the image (can leave as default)            | `""`                                          |
| `scripts`                                   | A list of scripts that the sensors will run                               | `[]`                                          |
| `scripts[].name`                            | The name of the script (must be unique)                                   | `""`                                          |
| `scripts[].script`                          | The bash code of the script                                               | `""`                                          |
| `sensors`                                   | A list of sensors to sample                                               | `[]`                                          |
| `sensors[].name`                            | The name of the sensor (must be unique)                                   | N/A                                           |
| `sensors[].scriptName`                      | The name of the script to run                                             | N/A                                           |
| `sensors[].params`                          | Parametes that will be given to the script as environment variables       | `{}`                                          |
| `sensors[].interval`                        | The interval this sensor will be sampled on                               | `"60s"`                                       |
| `sensors[].output_elasticsearch.index_name` | The elasticsearch index to send the results to                            | N/A                                           |
| `output.elasticsearch.host`                 | The elasticsearch host to send the results to                             | `""`                                          |
| `output.elasticsearch.port`                 | The elasticsearch port to send the results to                             | `9200`                                        |
| `output.elasticsearch.retry_limit`          | When failing to send data to elasticsearch, retry this number of times    | `3`                                           |
| `output.elasticsearch.retry_wait`           | The time in seconds to wait between retries                               | `1`                                          |
| `podAnnotations`                            | Annotations to give all created pods                                      | `{}`                                          |
| `podLabels`                                 | Labels to give all created pods                                           | `{}`                                          |
| `deployentAnnotations`                      | Annotations to give created deployent                                     | `{}`                                          |
| `deployentLabels`                           | Labels to give created deployent                                          | `{}`                                          |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`. For example:

```console
$ helm install sense8 sagbot/sense8 --set output.elasticsearch.port=9200
```

Alternatively, a YAML file that specifies the values for the parameters can be provided while
installing the chart. For example:

```console
$ helm install sense8 sagbot/sense8 --values values.yaml
```

In order to get started, you can use the example [```values.yaml``` file](https://gitlab.com/sagbot/helm-repo/-/raw/master/charts/sense8/values.yml)
