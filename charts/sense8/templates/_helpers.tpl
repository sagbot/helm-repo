{{/*The name of the chart that will be used as a prefix object names*/}}
{{- define "sagbot.blackboxer.name" -}}
{{- .Chart.Name | default .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end -}}

{{/* The name + version of the helm chart */}}
{{- define "sagbot.blackboxer.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "sagbot.blackboxer.imageRepository" -}}
{{- .Values.imageRepository | default "registry.gitlab.com/sagbot/blackbox-docker" }}
{{- end }}

{{- define "sagbot.blackboxer.imageTag" -}}
{{- .Values.imageTag | default "v1.12.4-debian-debian-1.0" }}
{{- end }}

{{- define "sagbot.blackboxer.imagePullPolicy" -}}
{{- .Values.imagePullPolicy | default "IfNotPresent" }}
{{- end }}





{{/*Global annotations for every object*/}}
{{- define "sagbot.blackboxer.globalAnnotations" -}}
sagbot/author: "Sagiv Oulu"
sagbot/chart: blackboxer
{{- end -}}

{{/*Global labels for every object*/}}
{{- define "sagbot.blackboxer.globalLabels" -}}
sagbot/chart: blackboxer
{{- end -}}

{{- define "sagbot.blackboxer.pullUsingCredentials" -}}
{{- if .Values.image.credentials -}}
true
{{- else -}}
false
{{- end -}}
{{- end -}}